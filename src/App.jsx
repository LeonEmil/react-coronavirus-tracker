import React from 'react';
//import Row from 'react-bootstrap/Row'
//import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import CardColumns from 'react-bootstrap/CardColumns'
import Form from 'react-bootstrap/Form'
import axios from "axios/dist/axios"

import Countries from './components/Countries'

import 'bootstrap/dist/css/bootstrap.min.css'
import "./App.css"
import addNumberPoints from './settings/functions'

class App extends React.Component {
  state = {
    api: "https://corona.lmao.ninja/v3/covid-19/all",
    video: "https://www.youtube.com/watch?v=mhA11RJMHEM",
    worldData: [],
    countriesData: [],
    searchCountries: [],
    componentsData : []
  }
  
  componentDidMount(){
    axios.all([
      axios.get("https://corona.lmao.ninja/v3/covid-19/all"), 
      axios.get("https://corona.lmao.ninja/v3/covid-19/countries")
    ])
    .then(resp => {
      this.setState({
        worldData: [
            {
              key: 0,
              title: "Cases",
              data: addNumberPoints(resp[0].data.cases),
              update: new Date(resp[0].data.updated).toString(),
              bg: "primary",
            },
            {
              key: 1,
              title: "Deaths",
              data: addNumberPoints(resp[0].data.deaths),
              update: new Date(resp[0].data.updated).toString(),
              bg: "primary",
            },
            {
              key: 2,
              title: "Recovered",
              data: addNumberPoints(resp[0].data.recovered),
              update: new Date(resp[0].data.updated).toString(),
              bg: "primary",
            }
          ],

          countriesData: resp[1].data
      })
    })
  }
  
  componentDidUpdate(){
    axios.all([
      axios.get("https://corona.lmao.ninja/v3/covid-19/all"),
      axios.get("https://corona.lmao.ninja/v3/covid-19/countries")
    ])
      .then(resp => {
        this.setState({
          worldData: [
            {
              key: 0,
              title: "Cases",
              data: addNumberPoints(resp[0].data.cases),
              update: new Date(resp[0].data.updated).toString(),
              bg: "primary",
            },
            {
              key: 1,
              title: "Deaths",
              data: addNumberPoints(resp[0].data.deaths),
              update: new Date(resp[0].data.updated).toString(),
              bg: "primary",
            },
            {
              key: 2,
              title: "Recovered",
              data: addNumberPoints(resp[0].data.recovered),
              update: new Date(resp[0].data.updated).toString(),
              bg: "primary",
            }
          ],

          countriesData: resp[1].data
        })
      })
  }
  
  render(){

      return (
        <>
        <div className="container">
          <header className="header">
            <h1>Coronavirus live tracker</h1>
          </header>
          <CardDeck>
            {
              this.state.worldData.map((component, key) => {
                return(
                  <Card key={key} className="text-center" bg={component.bg} text="white" style={{margin: "10px"}}>
                      <Card.Body>
                        <Card.Title>{component.title}</Card.Title>
                        <Card.Text>
                          {component.data}
                        </Card.Text>
                      </Card.Body>
                      <Card.Footer>
                      <small>Last updated: {component.update}</small>
                      </Card.Footer>
                    </Card>
                )
              })
            }
          </CardDeck>

          <Form>
            <Form.Group controlId="formGroupSearch">
              <Form.Label>Search country</Form.Label>
              <Form.Control type="search" placeholder="Country name" onChange={e => {
                this.setState({
                  searchCountries: e.target.value.toLowerCase()
                })
              }}/>
            </Form.Group>
          </Form>

        </div>
        <div className="container-fluid">
            <CardColumns>
          {
            <Countries countriesData={this.state.countriesData} searchCountries={this.state.searchCountries} />
          }
            </CardColumns>
        </div>
        </>
      );
    }
  }

export default App;
