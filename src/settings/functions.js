
const addNumberPoints = number => {
    let stringNumber = number.toString()
    stringNumber = stringNumber.split("")
    
    if(stringNumber.length > 3){
        stringNumber = stringNumber.reverse()
        for(let i = 3; i < stringNumber.length; i = i + 4){
            stringNumber.splice(i, 0, ".")
        }
        stringNumber = stringNumber.reverse()
        stringNumber = stringNumber.join("")
        return stringNumber
    }
    else {
        stringNumber = stringNumber.join("")
        return stringNumber
    }
}

export default addNumberPoints
