import React from 'react'
import Card from 'react-bootstrap/Card'
import addNumberPoints from '../settings/functions.js'

const Countries = ({countriesData, searchCountries}) => {

    const filterCountries = countriesData.filter(item => {
        return searchCountries !== "" ? item.country.toLowerCase().startsWith(searchCountries) : item 
    })

    return (
        filterCountries.map((component, key) => {
            return (
                <Card key={key}>
                    <Card.Body className="Card-country__body">
                        <Card.Img src={component.countryInfo.flag} className="Card-country__image" />
                        <Card.Title>{component.country}</Card.Title>
                        <Card.Text>Cases: {addNumberPoints(component.cases)}</Card.Text>
                        <Card.Text>Deaths: {addNumberPoints(component.deaths)}</Card.Text>
                        <Card.Text>Recovered: {addNumberPoints(component.recovered)}</Card.Text>
                    </Card.Body>
                </Card>
            )
        })
    );
}
 
export default Countries;
